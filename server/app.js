// 引入模块 //
const express = require('express'),
      bodyParser = require('body-parser'),
      path = require('path')
// 初始化
const app = express();
// express设置 - POST_GET设置, 静态目录
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.join(__dirname, '../dist/public')));
// 监听端口
app.listen(8000)
// 加载路由器规则文件 & 数据库规则文件
const route_config = require('./server/config/routes')
      models = require('./server/config/models')
// 在这里目录规则_使用这个文件
app.use('/',route_config);   //前台(路由)  http://localhost:8000/

/*
    -- 以后需要可以在进行更仔细的模块化 --

    var test = require('./server/config/test')
    var admin = require('./server/config/admin')

    app.use('/test',test);   //前台(路由)  http://localhost:8000/test
    app.use('/admin',admin);   //前台(路由)  http://localhost:8000/admin
*/

// this route will be triggered if any of the routes above did not match
app.all("*", (req,res,next) => {
	res.sendFile(path.resolve("../dist/public/index.html"))
});
