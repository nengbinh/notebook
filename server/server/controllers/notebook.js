/*                      地址: http://localhost:8000/notebook              */

// 加载模块
const express=require('express'),
      mongoose = require('../config/models')
// 初始化
const router = express.Router(),
      notebook = mongoose.model('notebook')

/*                                  路由器规则                             */

// add new notes
router.post('/newNote', (req,res)=>{
    notebook.create(req.body, (err,data)=>{
        res.json(data)
    })
})

// read notes
router.get('/', (req,res)=>{
    notebook.find({}, (err,data)=>{
        res.json(data)
    })
})

// read notes detail 
router.get('/:id', (req,res)=>{
    notebook.findOne({_id: req.params.id}, (err,data)=>{
        res.json(data)
    })
})

// update notes
router.post('/updateNote/:id', (req,res)=>{
    notebook.updateOne({_id: req.params.id},req.body,(err,data)=>{
        res.json(data)
    })
})

// delete notes
router.delete('/:id', (req,res)=>{
    notebook.deleteOne({_id: req.params.id}, (err,data)=>{
        res.json(data)
    })
})

// 暴露接口
module.exports = router
