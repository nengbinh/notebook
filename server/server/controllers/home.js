/*                      地址: http://localhost:8000/                     */

// 加载模块
const express=require('express')
// 初始化
const router = express.Router()

/*                                  路由器规则                             */

router.get('/',(req,res)=>{
    res.render('index')
})

router.get('/tasks',(req,res)=>{
    res.json({user:1})
})

router.get('/example2',(req,res)=>{
    res.send('Hi, this is example2')
})

router.get('/getAPI', (req,res)=>{
    const axios = require('axios');
    //
    axios.get('https://samples.openweathermap.org/data/2.5/weather?q=London,uk&appid=b6907d289e10d714a6e88b30761fae22')
    .then(data => {
        res.json(data.data);
    })
    .catch(error => {
        res.json(error);
    })
})

// 暴露接口
module.exports = router