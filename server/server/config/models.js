/*                                模型总文件                              */

// 引入模块
const mongoose = require('mongoose'),
      path = require('path'),
      fs = require('fs')
// 连接到数据库
mongoose.connect('mongodb://localhost/my_first_db',{ useNewUrlParser: true });

// 暴露接口
module.exports = mongoose

// 加载所有的model文件
const models_path = path.join(__dirname, '../models');
fs.readdirSync(models_path).forEach(function(file) {
    if(file.indexOf('.js') >= 0) {
        require(models_path + '/' + file);
    }
})

