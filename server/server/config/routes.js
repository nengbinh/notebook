/*                                路由器的控制文件                          */

// 加载模块
const express=require('express')
// 初始化
const router = express.Router()
// 加载控制器
const home = require('../controllers/home'),
      notebook = require('../controllers/notebook')
// 规则
router.use('/', home)               // http://localhost:8000/
router.use('/notebook', notebook)        // http://localhost:8000/notebook

// 暴露接口
module.exports = router