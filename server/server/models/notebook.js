// 引入模块
const mongoose = require('../config/models')

// 建立表模型
const Model = new mongoose.Schema({
    title: {type:String},
    content: {type:String, default:''},
}, {timestamps : true})


// 表绑定上模型
mongoose.model('notebook', Model);

// 暴露接口
module.exports = mongoose