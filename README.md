# 路由 Router

[例子在]
app.component.html : `1-13行`  
app.module.ts : `12-14行` 创建component的时候自动加进去的 `ng g c component_name`  
app.routing.module.ts : `5-13行` [要对应app.module.ts的命令名字]  
分别对于src/app 路径下的beijing, guangzhou, shanghai文件夹  

# 建立全组件能使用的服务数据

#### 创建服务
我们将通过导航到我们终端中的Angular应用程序并运行此行来设置我们的服务：  
`ng g s mydata`  

##### 文件 (.../app/app.module.ts)  
import { MydataService} from './mydata.service';  
  
@NgModule({  
        ...  
        providers: [MydataService],  
        ...  
})  
  
打开你的 `mydata.service.ts` 文件  
import { HttpClient } from '@angular/common/http';  
constructor(private _http: HttpClient) {  
}  
在component里面引入 import { MydataService } from '../mydata.service';  
在 constructor里面加入 private MyData: MydataService 就可以使用了  
 


# 嵌套组件 Nested components

[例子在]
app.component.html : `33行`  
app.module.ts : `11行` 创建component的时候自动加进去的 `ng g c component_name`  
app.routing.module.ts : `5-13行` [要对应app.module.ts的命令名字]  
分别对于src/app 路径下的fish文件夹  

# 父组件传递参数到子组件

[例子在]
src/app/fish 路径下的 `fish.component.ts文件`,`fish.component.html文件`  
app.component.html : `15-33行`  
[由于taskToShow任务组件中的selectedTask数据绑定到父组件中的  数据，因此任何更改都  selectedTask将立即反映出来,taskToShow反之亦然]  
[在*ngFor里面直接赋值val的值可以调动里面的数组数据]  

# Observables
当我们使用jQuery来处理我们的AJAX请求时，代码会变得相当混乱。Angular通过使用Observables使代码更加清晰。

#### 创建服务 
`ng g s http` 我们将服务命名为“http”，但您可以根据自己的喜好命名。这给了我们一个名为`http.service.ts`的文件  
`app/app.module.ts 7,30行`  

#### HttpClient
我们提供了服务，以便它可以从我们的数据库中获取数据，但它不能这样做，除非它可以发出http请求！因此，我们需要我们的项目来导入HttpClientModule。让我们导入它并将其包含在导入数组中。  
`app/app.module.ts 8,25,26 行`  

#### 依赖注入
当应用程序的一部分依赖于另一部分时，我们使用依赖注入。  
`app/http.service.ts 2,9 行` 必须  
我们需要的另一个依赖注入是将服务注入组件。除非组件需要，否则不会使用服务，所以让我们打开我们的根组件的.ts文件。导入服务并使其成为类中的属性。  
`app/app.component.ts 2,21 行`  

#### 从数据库中获取数据
现在我们都准备开始从服务中发出http请求了！让我们首先在Restful Task API分配中使用路由来获取所有任务。  
`app/http.service.ts 11-25 行`  

#### 使用
`app/app.component.ts 50-96 行`  

#### 最终发送的数据是在express的controller文件里面
`server/server/controllers/rest.js`  

# ngOnInit 

我们将从组件的ngOnInit方法中调用它。该ngOnInit方法的构造方法后立即调用。  

`app/app.component.ts 1,11,25 行`

# 变量

`app.component.ts 14-18 行  
`app.component.html 50 行`  

# *ngIf *ngFor

`app.component.html 26,49 行`

# 事件 event
`app.component.html 52 行`  
`app.component.ts 36-96 行`  

# 跨域访问 Cors
`app.component.html 72,73 行`  
[app/cors]文件夹的 `cors.component.html`跟`cors.component.ts`文件  
`http.service.ts 27-29 行`  
[controller]里面的`home.js 22-32 行`  

# 自定义PIPE
CLI命令 `ng g p sexReform`  
`app.component.html 68,69 行`  
`sex-reform.pipe.ts 文件`  
 
# Public

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.2.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
