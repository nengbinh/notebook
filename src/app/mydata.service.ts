import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { DomSanitizer } from '@angular/platform-browser';


@Injectable({
  providedIn: 'root'
})
export class MydataService {

  note: any; // note data to be showning
  noteList: any = []; // this is the data will be show at list
  noteId: string;
  preview: any;
  help: boolean = false;

  hint_color: string;
  hint: string;

  constructor(
        private _http: HttpClient,
        private sanitizer: DomSanitizer
    ) {
    this.newNote();
  }

  // new note
  newNote() {
    this.note = {
      from: 'new',
      title: '',
      content: '',
    };
    this.noteId = '';
    this.setHint('orange', 'new note');
  }

  // help window
  helpPOP() {
    this.help = this.help ? false : true;
  }

  // set new note
  newNoteButton() {
    if (!this.changeCheck()) { return false; }
    this.newNote();
    this.preview_acti();
  }

  // check change
  changeCheck() {
    if (this.hint === 'unsaved') {
      return confirm('Note have been changed, Are you sure to leave?');
    } else {
      return true;
    }
  }

  // hint set
  setHint(color, text) {
    this.hint_color = color;
    this.hint = text;
  }

  // save note
  saveNote() {
    if (this.note.from === 'new') {
      this.addNewNote();
    } else {
      this.addUpdateNote();
    }
  }

  // update note
  addUpdateNote() {
    this._http.post('/notebook/updateNote/' + this.noteId, this.note)
    .subscribe(data => {
      this.readNote();
      this.setHint('green', 'saved');
    });
  }

  // new note add
  addNewNote() {
    this._http.post('/notebook/newNote', this.note)
    .subscribe(data => {
      this.readNote();
      this.newNote();
    });
  }

  // read my note
  readNote() {
    this._http.get('/notebook')
    .subscribe(data => {
      this.noteList = data;
    });
  }

  // read notes from list
  readNoteDetail(id) {
    if (!this.changeCheck()) { return false; }
    this._http.get('/notebook/' + id)
    .subscribe(data => {
      this.setHint('orange', 'read note');
      this.noteId = id;
      this.note.from = 'read';
      this.note.title = data['title'];
      this.note.content = data['content'];
      this.preview_acti();
    });
  }

  // delete notes
  noteDelete() {
    if (!confirm('Are you sure?')) { return false; }
    this._http.delete('/notebook/' + this.noteId)
    .subscribe(data => {
      this.newNote();
      this.readNote();
    });
  }

  // preview activaty
  preview_acti() {
    const temp = this.preview_process(this.note.content);
    this.preview = this.sanitizer.bypassSecurityTrustHtml(temp);
  }

  ///////////////////////////////////////////////////////////////////

  // sync to preview
  preview_show() { // when edit changed then trigger this
    this.setHint('red', 'unsaved');
    const temp = this.preview_process(this.note.content); // convert from here
    this.preview = this.sanitizer.bypassSecurityTrustHtml(temp);
  }

  // process preview
  preview_process(text) { // convert from here
    let out = '';
    for (let i = 0; i < text.length; i++) {
      const received = this.prv_checkword(text[i], i, text);
      i += received['inc'];
      out += received['text'];
    }
    //
    return out;
  }

  // check word
  prv_checkword(word, from, text) {
    const re = { inc: 0, text: '' };
    let back;
    switch (word) {
      case '#': // header
        back = this.prv_header(from, text);
      break;

      case '`': // high light
        back = this.prv_highlight(from, text);
      break;

      case ' ': // change line
        back = this.prv_changeline(from, text);
      break;

      case '*': // bold Italic Dividing
        if (text[from + 1] === '*' && text[from + 2] === '*') {
          back = this.prv_dividing(from, text);
        } else if (text[from + 1] === '*') {
          back = this.prv_bold(from, text);
        } else {
          back = this.prv_italic(from, text);
        }
      break;

      case '~': // delete
        if (text[from + 1] === '~') { back = this.prv_delete(from, text); }
      break;
    }
    //
    if (back) {
      // if got something
      re['inc'] = back['inc'];
      re['text'] = back['text'];
    } else {
      // stay same
      re['text'] = word;
    }
    return re;
  }

  ///////////////////////////////////////////////

  // header
  prv_header(from, text) {
    let word = '';
    // check the stop point
    for (let i = from; i < text.length; i++) {
      if ( text[i] === '\n' ) { break; }
      word += text[i];
    }
    // check how many #
    let count = 0;
    for (let i = 0; i < word.length; i++  ) {
      if (word[i] !== '#' || count >= 6) { break; }
      count ++;
    }
    // combined
    return {
      inc: word.length,
      text: `<h${count} _ngcontent-c6 class='header'>${word.substr(count)}</h${count}>`
    };
  }

  // bold
  prv_bold(from, text) {
    // do not do next line
    text = text.substr(from + 2).split('\n')[0];
    text = text.split('  ')[0];
    // check if exist
    const stop = text.indexOf('**');
    //
    return stop > -1 ?
      {
        inc: stop + 3,
        text: `<strong>${text.substr(0, stop) }</strong>`
      }
    : {
        inc: 0,
        text: '*'
      };
  }

  // italic
  prv_italic(from, text) {
    // do not do next line
    text = text.substr(from + 1).split('\n')[0];
    text = text.split('  ')[0];
    // check if exist
    const stop = text.indexOf('*');
    //
    return stop > -1 ?
      {
        inc: stop + 1,
        text: `<i>${text.substr(0, stop) }</i>`
      }
    : {
        inc: 0,
        text: '*'
      };
  }

  // delete
  prv_delete(from, text) {
    // do not do next line
    text = text.substr(from + 2).split('\n')[0];
    text = text.split('  ')[0];
    // check if exist
    const stop = text.indexOf('~~');
    //
    return stop > -1 ?
      {
        inc: stop + 3,
        text: `<s>${text.substr(0, stop) }</s>`
      }
    : {
        inc: 0,
        text: '~'
      };
  }

  prv_dividing(from, text) {
    return { inc: 2, text: `<hr _ngcontent-c6 style='margin:10px 0'>` };
  }

  // high light
  prv_highlight(from, text) {
    let word = '';
    let activaty = false;
    // check the stop point
    for (let i = from; i < text.length; i++) {
      if (i === from) {continue; }
      if ( text[i] === '`' ) { activaty = true; break; }
      word += text[i];
    }
    // combined
    return activaty ? {
      inc: word.length + 1,
      text: `<span _ngcontent-c6 class='hightlight'>${word}</span>`
    } : {
      inc: 0,
      text: text[from]
    };
  }

  // change line
  prv_changeline(from, text) {
    let activaty = false;
    if (text[from + 1] === ' ') {
      activaty = true;
    }
    // combined
    return activaty ? {
      inc: 0,
      text: `<br>`
    } :
    {
      inc: 0,
      text: ` `
    };
    //
  }

}
