import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class HttpService {
  constructor(private _http: HttpClient) {}

  getAll(userid?: any) {
    return this._http.get('/rest', {params: userid});
  }

  createNew(newObj: object) {
    return this._http.post('/rest/add', newObj);
  }

  deleteOne(userid: any) {
    return this._http.delete('/rest', {params: {_id: userid}});
  }

  updateOne(userid: any, update: object) {
    return this._http.put(`/rest`, update, {params: {_id: userid}});
  }

  getAPI() {
    return this._http.get(`/getapi`);
  }

}
