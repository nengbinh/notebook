import { Component, OnInit } from '@angular/core';
import { MydataService } from './mydata.service';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: [
    './app.component.css'
  ]
})

export class AppComponent implements OnInit {
  title = 'My NoteBook';
  // run first
  constructor(public mydata: MydataService) { 
  }

  // run after constructor
  ngOnInit() {
    console.log(this.mydata.help)
  }
  
}

