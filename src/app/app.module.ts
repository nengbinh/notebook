import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { HttpService } from './http.service';
import { HttpClientModule } from '@angular/common/http';

import { FormsModule } from '@angular/forms';
import { SexReformPipe } from './sex-reform.pipe';

import { MydataService} from './mydata.service';
import { NotesComponent } from './notes/notes.component';
import { EditComponent } from './edit/edit.component';
import { NavComponent } from './nav/nav.component';
import { ShowComponent } from './show/show.component';
import { PreviewComponent } from './preview/preview.component';
import { HelpComponent } from './help/help.component';

@NgModule({
  declarations: [
    AppComponent,
    SexReformPipe,
    NotesComponent,
    EditComponent,
    NavComponent,
    ShowComponent,
    PreviewComponent,
    HelpComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [HttpService, MydataService],
  bootstrap: [AppComponent]
})
export class AppModule { }
